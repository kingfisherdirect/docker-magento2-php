# Magento 2 PHP Docker image

This project aims to provide docker PHP images for use with Magento 2.3 and
newer.

Setup is following the Magento documentation regards PHP.

There are 3 purposes of those images:

- Development (php-fpm, to be used with nginx)
- Production (php-fpm)
- Production Cron

## Tags

- `latest` - latest production (fpm / cron) image
- `X` - major version production image
- `X.X` - minor version production image
- `X.X.X` - patch version production image
- `dev` - latest development image
- `X-dev` - major version development image
- `X.X-dev` - minor version development image
- `X.X.X-dev` - patch version development image

## Versions

- `4` - PHP 7.4 + Composer 2
- `3` - PHP 7.3 + Composer 1
- `2` - PHP 7.2 + Composer 1

## How to use this image

In order to use this image you'll have to put your Magento project files inside
the container. Depending on tag you may do that differently

### Production and Cron

For production and cron you should use this image as a base to extend.

The very basic `Dockerfile` example is:

```
FROM kingfisherdirect/magento2-php

# Copy application data (assuming magento data is in src folder)
COPY --chown=app:app src /var/www/html
```

To run the image you need to run it with following env variables. Almost all of
them are used in setup file ([magento-setup](https://bitbucket.org/kingfisherdirect/docker-magento2-php/src/master/bin/magento-setup))

- `MAGE__KEY` - Encryption key for Magento
- `MAGE__DB_HOST` - Database host
- `MAGE__DB_NAME` - Database name
- `MAGE__DB_USER` - Database username
- `MAGE__DB_PASSWORD` - Database password
- `MAGE__BACKEND_FRONTNAME` - Magento admin panel location
- `MAGE__CACHE_REDIS_SERVER` - (optional) Redis host for use as cache. If your setup requires you to put any value instead of leaving this empty you can use `0` to not disable this.
- `MAGE__PAGE_CACHE_REDIS_SERVER` - (optional) Redis host for use as page cache.
Can use value of `0` to disable.
- `MAGE__SESSION_REDIS_HOST` - (optional) Redis host for use as session storage.
Can use value of `0` to disable.
- `MAGE_DEPLOY__ALLOW_OUTDATED_DB` - (optional) whether dismiss error about need
for setup:upgrade run. Accepts `true`
- `MAGE_DEPLOY__UPGRADE` - (optional) whether allow running setup:upgrade.
This will not work if above is set to `true`. Accepts `true`
- `MAGE_DEPLOY__SKIP_SETUP` - (optional) if you want to skip Magento setup but
still use one of two entrypoints described below.
- `MAGE__INSTALL_DATE` - (optional) This will be used as install date in env.php. Default: `Tue, 28 May 2019 03:13:37 +0000`

Depending on whether you want to use it as php-fpm or cron you may use one of 2
available entrypoints:

- [magento-entrypoint](https://bitbucket.org/kingfisherdirect/docker-magento2-php/src/master/bin/magento-entrypoint) - php-fpm, or any custom command
- [cronstart](https://bitbucket.org/kingfisherdirect/docker-magento2-php/src/master/bin/cronstart) - just cron, doesn't pick-up any command

Alternatively you can use just `php-fpm` as entrypoint, but this will certainly
not work if you wont add env.php into the image. _(Development seutp uses that
option)_

### Development

For **development** you want to use `dev`/`*-dev` tags. That image should be used as
php-fpm service for use with nginx.

Dev setup usually requires access to files from both sides (host/docker), so
the best is to use docker volumes to bind host folder into containers
`/var/www/html` folder.

**Example docker-compose.yml**

```
version: "3"

services:
  phpfpm:
    image: kingfisherdirect/magento2-php:dev
    links:
      - db
    volumes:
      # your magento root folder must be mounted in `/var/www/html`
      - ./src:/var/www/html:delegated

      # it's usefull to mount your host composer folder, to reuse your auth keys and cache
      - ~/.composer:/var/www/.composer:delegated

      # (optional) all of the `/usr/local/etc/php/conf.d/*.ini` files are loaded as php ini configuration. You can put your custom configuration there
      - ./env/php/magento_dev.ini:/usr/local/etc/php/conf.d/magento_dev.ini:ro

  # nginx setup is here as an basic example, you should come up with your setup
  # rather than relaying on this
  nginx:
    image: nginx:latest
    ports:
      - "80:80"
    links:
      - phpfpm
    volumes:
      - ./src:/var/www/html:ro
      # nginx configuration (not included in this repo)
      - ./nginx/default.conf:/etc/nginx/conf.d/default.conf:ro

  db:
    …
    # it's really up to you
```

As this image uses `php-fpm` entrypoint you don't need any of environment
varialbes for setup.

#### PHP/Magento/Composer Commands

For the config above running any php or magento command should as easy as:

`docker-compose exec phpfpm COMMAND`

- **Magento** - `docker-compose exec phpfpm bin/magento`
- **Composer** - `docker-compose exec phpfpm composer`

#### Xdebug

It is installed in `dev` image, but it's not enabled by default.

You can enable it by adding a config file `/usr/local/etc/php/conf.d/` with your
docker-compose.yml file.

**Example configuration**

PHP is able to read ENV variables in ini files, therfore in this example I use
few variables to control Xdebug behaviour from docker-compose.yml file.

```
# docker-compose.yml

services:
  phpfpm:
    image: kingfisherdirect/magento2-php:dev
    ...
    environment:
      - PHP_XDEBUG_ENABLED=1
      - PHP_XDEBUG_REMOTE_PORT=9000
      - PHP_XDEBUG_REMOTE_HOST=host.docker.internal
    volumes:
      ...
      - ./path/to/xdebug.ini:/usr/local/etc/php/conf.d/xdebug.ini:ro
```

```
; xdebug.ini

zend_extension = xdebug.so

; either 1 / 0
xdebug.remote_enable = ${PHP_XDEBUG_ENABLED}
; either 1 / 0
xdebug.remote_autostart = ${PHP_XDEBUG_ENABLED}

xdebug.remote_port = ${PHP_XDEBUG_REMOTE_PORT}
xdebug.remote_host = ${PHP_XDEBUG_REMOTE_HOST}
```

Xdebug will be then accessible for IDEs on `localhost:9000`.

**Host Note:** On Linux Docker `host.docker.internal` is not working. You have
to provide an IP address that resolves to a host system. Look into setting up a
loopback address.
