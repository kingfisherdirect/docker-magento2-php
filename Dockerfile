FROM php:8.2-fpm AS magento_php_base
LABEL maintainer="kamil@kingfisherdirect.co.uk"

# default value
ENV PHP_MEMORY_LIMIT=1G

RUN useradd -m -u 1000 -s /bin/sh app

# composer:          unzip git
# gd dependencies:   libfreetype6-dev libjpeg62-turbo-dev libpng-dev
# intl dependendies: libicu-dev
# simplexml:         libxml2
# xsl:               libxslt-dev
# sodium:            libsodium-dev
# zip:               libzip-dev
# imagick:           libmagickwand-dev

RUN apt-get update \
  && apt-get install --no-install-recommends --yes \
    git \
    libcap2-bin \
    libfreetype6-dev libjpeg62-turbo-dev libpng-dev \
    libicu-dev \
    libmagickwand-dev \
    libsodium-dev \
    libxml2 \
    libxslt-dev \
    libzip-dev \
    mailutils \
    ssmtp \
    unzip \
  && rm -rf /var/lib/apt/lists/*

RUN pecl install -f apcu igbinary imagick redis

RUN docker-php-ext-configure zip \
  && docker-php-ext-configure gd --with-freetype --with-jpeg \
  && docker-php-ext-install -j$(nproc) \
    bcmath \
    gd \
    intl \
    opcache \
    pdo_mysql \
    simplexml \
    soap \
    sockets \
    xsl \
    zip \
  && docker-php-ext-enable \
    apcu \
    igbinary \
    imagick \
    redis

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --2.2 --filename=composer

COPY bin/* /usr/local/bin/
COPY config/zz_www.conf /usr/local/etc/php-fpm.d/

RUN chmod +x \
  /usr/local/bin/cron-entrypoint \
  /usr/local/bin/cron-run \
  /usr/local/bin/magento-entrypoint \
  /usr/local/bin/magento-setup

ENTRYPOINT ["magento-entrypoint"]

WORKDIR /var/www/html

#
# Main Production Image
#
FROM magento_php_base as magento_php

COPY ["config/magento.ini", "config/magento_prod.ini", "$PHP_INI_DIR/conf.d/"]

# default to php production settings
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
RUN sed -i "s/expose_php = On/expose_php = Off/g" "$PHP_INI_DIR/php.ini"

VOLUME /var/www/html/pub/media

USER app:app

#
# Dev Image
#
FROM magento_php_base AS magento_php_dev

COPY config/magento.ini $PHP_INI_DIR/conf.d/

# default to php development settings
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

RUN pecl install xdebug && pecl install excimer

USER app:app
